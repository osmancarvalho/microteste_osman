package teste.br.edu.ifs.aulas.testesoftware.exercicio02;

import br.edu.ifs.aulas.testesoftware.exercicio02.*;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

/**
 * Created by Osman Junior on 29/11/2016.
 */

public class IArvoreBinariaBuscaTest {

    Object elemento;
    Object[] elementos;

    @Mock
    IArvoreBinariaBusca arvore = mock(IArvoreBinariaBusca.class);
    No raiz = mock(No.class);
    No no1 = mock(No.class), no2 = mock(No.class), no3 = mock(No.class), no4 = mock(No.class);


    @Before
    public void setUp() throws Exception {
        when(no1.getInfo()).thenReturn(20);
        when(no2.getInfo()).thenReturn(7);
        when(no3.getInfo()).thenReturn(30);
        when(no4.getInfo()).thenReturn(5);
        when(raiz.getDireita()).thenReturn(no1);
        when(raiz.getEsquerda()).thenReturn(no2);
        when(no1.getDireita()).thenReturn(no3);
        when(no2.getEsquerda()).thenReturn(no4);
    }

    @Test
    public void estahCheiaTest() throws Exception {

    }

    @Test
    public void estahCompletaTest() throws Exception {

    }

    @Test
    public void getSubArvoreDireitaTest() throws Exception {
        when(raiz.getInfo()).thenReturn(10);

        Answer<Object> subArvoreRaiz = new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                No aux = raiz;
                elementos = new Object[5];
                int i = 0;
                while (aux.getDireita() != null) {
                    elementos[i] = aux.getDireita().getInfo();
                    aux = aux.getDireita();
                    i++;
                }
                return null;
            }
        };
        doAnswer(subArvoreRaiz).when(arvore).getSubArvoreDireita(raiz);

        arvore.getSubArvoreDireita(raiz);
        assertThat(20, is(elementos[0]));
        assertThat(30, is(elementos[1]));
        assertNull(elementos[2]);

        Answer<Object> subArvoreNo1 = new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                No aux = no1;
                elementos = new Object[5];
                int i = 0;
                while (aux.getDireita() != null) {
                    elementos[i] = aux.getDireita().getInfo();
                    aux = aux.getDireita();
                    i++;
                }
                return null;
            }
        };

        doAnswer(subArvoreNo1).when(arvore).getSubArvoreDireita(no1);
        arvore.getSubArvoreDireita(no1);
        assertThat(30, is(elementos[0]));
        assertNull(elementos[1]);
    }

    @Test(expected = NoNaoEncontradoException.class)
    public void getSubArvoreDireitaExceptionTest() throws Exception {
        Answer<Object> subArvoreNo3 = new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                No aux = no3;
                elementos = new Object[5];
                int i = 0;
                if (no3.getDireita() != null) {
                    while (aux.getDireita() != null) {
                        elementos[i] = aux.getDireita().getInfo();
                        aux = aux.getDireita();
                        i++;
                    }
                } else {
                    throw new NoNaoEncontradoException();
                }
                return null;
            }
        };

        doAnswer(subArvoreNo3).when(arvore).getSubArvoreDireita(no3);
        arvore.getSubArvoreDireita(no3);
    }

    @Test
    public void getSubArvoreEsquerdaTest() throws Exception {
        when(raiz.getInfo()).thenReturn(10);

        Answer<Object> subArvoreRaiz = new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                No aux = raiz;
                elementos = new Object[5];
                int i = 0;
                while (aux.getEsquerda() != null) {
                    elementos[i] = aux.getEsquerda().getInfo();
                    aux = aux.getEsquerda();
                    i++;
                }
                return null;
            }
        };
        doAnswer(subArvoreRaiz).when(arvore).getSubArvoreEsquerda(raiz);

        arvore.getSubArvoreEsquerda(raiz);
        assertThat(7, is(elementos[0]));
        assertThat(5, is(elementos[1]));
        assertNull(elementos[2]);

        Answer<Object> subArvoreNo2 = new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                No aux = no2;
                elementos = new Object[5];
                int i = 0;
                while (aux.getEsquerda() != null) {
                    elementos[i] = aux.getEsquerda().getInfo();
                    aux = aux.getEsquerda();
                    i++;
                }
                return null;
            }
        };

        doAnswer(subArvoreNo2).when(arvore).getSubArvoreEsquerda(no2);
        arvore.getSubArvoreEsquerda(no2);
        assertThat(5, is(elementos[0]));
        assertNull(elementos[1]);
    }

    @Test(expected = NoNaoEncontradoException.class)
    public void getSubArvoreEsquerdaExceptionTest() throws Exception {
        Answer<Object> subArvoreNo4 = new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                No aux = no4;
                elementos = new Object[5];
                int i = 0;
                if (no4.getEsquerda() != null) {
                    while (aux.getEsquerda() != null) {
                        elementos[i] = aux.getEsquerda().getInfo();
                        aux = aux.getEsquerda();
                        i++;
                    }
                } else {
                    throw new NoNaoEncontradoException();
                }
                return null;
            }
        };

        doAnswer(subArvoreNo4).when(arvore).getSubArvoreDireita(no4);
        arvore.getSubArvoreDireita(no4);
    }

    @Test
    public void inserirNaArvoreVaziaTest() throws Exception {
        assertNull(raiz.getInfo());
        final Answer<Object> answer1 = new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                elemento = 8;
                when(raiz.getInfo()).thenReturn(elemento);
                return raiz.getInfo();
            }
        };
        doAnswer(answer1).when(arvore).inserir(elemento);

        arvore.inserir(elemento);
        assertThat(8, is(raiz.getInfo()));
    }

    @Test
    public void inserirNaArvoreInicializadaTest() throws NullPointerException {
        Answer<Object> setDireita;
        Answer<Object> setEsquerda;

        when(raiz.getInfo()).thenReturn(10);

        setDireita = new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                when(raiz.getDireita()).thenReturn(no1);
                return null;
            }
        };

        setEsquerda = new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                when(raiz.getEsquerda()).thenReturn(no2);
                return null;
            }
        };

        doAnswer(setDireita).when(arvore).inserir(no1);
        doAnswer(setEsquerda).when(arvore).inserir(no2);

        arvore.inserir(no1);
        arvore.inserir(no2);

        assertThat(20, is(raiz.getDireita().getInfo()));
        assertThat(7, is(raiz.getEsquerda().getInfo()));
    }

    @Test
    public void getTipoNoTest() throws Exception {
        TipoNo tipono1, tipono2 = null, tipono3 = null, tipono4 = null;

        if (no1.getDireita() == null & no1.getEsquerda() == null) {
            tipono1 = TipoNo.Externo;
        } else {
            tipono1 = TipoNo.Interno;
            if (no2.getDireita() == null & no2.getEsquerda() == null) {
                tipono2 = TipoNo.Externo;
            } else {
                tipono2 = TipoNo.Interno;
            }
            if (no3.getDireita() == null & no3.getEsquerda() == null) {
                tipono3 = TipoNo.Externo;
            } else {
                tipono3 = TipoNo.Interno;
            }
            if (no4.getDireita() == null & no4.getEsquerda() == null) {
                tipono4 = TipoNo.Externo;
            } else {
                tipono4 = TipoNo.Interno;
            }
        }

        doReturn(tipono1).when(arvore).getTipoNo(no1);
        doReturn(tipono2).when(arvore).getTipoNo(no2);
        doReturn(tipono3).when(arvore).getTipoNo(no3);
        doReturn(tipono4).when(arvore).getTipoNo(no4);

        arvore.getTipoNo(no1);
        arvore.getTipoNo(no2);
        arvore.getTipoNo(no3);
        arvore.getTipoNo(no4);

        assertThat(TipoNo.Interno, is(arvore.getTipoNo(no1)));
        assertThat(TipoNo.Interno, is(arvore.getTipoNo(no2)));
        assertThat(TipoNo.Externo, is(arvore.getTipoNo(no3)));
        assertThat(TipoNo.Externo, is(arvore.getTipoNo(no4)));
    }

    @Test(expected = NoNaoEncontradoException.class)
    public void getTipoNoExceptionTest() throws Exception {
        when(raiz.getInfo()).thenReturn(10);
        when(arvore.getTipoNo(no1)).thenThrow(new NoNaoEncontradoException());
        arvore.getTipoNo(no1);
    }

    @Test
    public void getAlturaTest() throws Exception {

    }

    @Test
    public void getNivelTest() throws Exception {

    }

    @Test
    public void getElementosTest() throws Exception {
        when(raiz.getInfo()).thenReturn(10);
        Object[] preOrdem = new Object[5];
        preOrdem[0] = no4.getInfo();
        preOrdem[1] = no2.getInfo();
        preOrdem[2] = raiz.getInfo();
        preOrdem[3] = no1.getInfo();
        preOrdem[4] = no3.getInfo();
        when(arvore.getElementos(TipoPercurso.PreOrdem)).thenReturn(preOrdem);

        arvore.getElementos(TipoPercurso.PreOrdem);
        assertThat(5, is(preOrdem[0]));
        assertThat(7, is(preOrdem[1]));
        assertThat(10, is(preOrdem[2]));
        assertThat(20, is(preOrdem[3]));
        assertThat(30, is(preOrdem[4]));

        Object[] inOrdem = new Object[5];
        inOrdem[0] = no4.getInfo();
        inOrdem[1] = no2.getInfo();
        inOrdem[2] = raiz.getInfo();
        inOrdem[3] = no3.getInfo();
        inOrdem[4] = no1.getInfo();
        when(arvore.getElementos(TipoPercurso.InOrdem)).thenReturn(inOrdem);

        arvore.getElementos(TipoPercurso.InOrdem);

        assertThat(5, is(inOrdem[0]));
        assertThat(7, is(inOrdem[1]));
        assertThat(10, is(inOrdem[2]));
        assertThat(30, is(inOrdem[3]));
        assertThat(20, is(inOrdem[4]));


        Object[] posOrdem = new Object[5];
        posOrdem[0] = no4.getInfo();
        posOrdem[1] = no2.getInfo();
        posOrdem[2] = no3.getInfo();
        posOrdem[3] = no1.getInfo();
        posOrdem[4] = raiz.getInfo();
        when(arvore.getElementos(TipoPercurso.PosOrdem)).thenReturn(posOrdem);
 
        System.out.println();
        arvore.getElementos(TipoPercurso.PosOrdem);

        assertThat(5, is(posOrdem[0]));
        assertThat(7, is(posOrdem[1]));
        assertThat(30, is(posOrdem[2]));
        assertThat(20, is(posOrdem[3]));
        assertThat(10, is(posOrdem[4]));

    }

    @Test
    public void removerTest() throws Exception {

    }

    @Test
    public void localizarTest() throws Exception {

    }

}
