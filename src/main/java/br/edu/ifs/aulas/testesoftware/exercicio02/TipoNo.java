package br.edu.ifs.aulas.testesoftware.exercicio02;

/**
 * Created by Francisco on 28/11/2016.
 */
public enum TipoNo {
    Interno(1),
    Externo(2);

    private final int valor;
    private TipoNo(int valor){
        this.valor = valor;
    }
}
