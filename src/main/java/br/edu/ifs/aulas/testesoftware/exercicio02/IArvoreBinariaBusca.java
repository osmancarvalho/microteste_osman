package br.edu.ifs.aulas.testesoftware.exercicio02;

/**
 * Created by Francisco on 28/11/2016.
 */
public interface IArvoreBinariaBusca {
    Object getRoot();

    /**
     * É dita como árvore cheia se todas as folhas estão no mesmo nível e todos os nós não-folhas tem dois filhos.
     * Quando uma árvore está cheia o nível N tem 2n nós
     * @return
     */
    boolean estahCheia();

    /**
     * É dita completa se ela for cheia até o nível h-1 e os nós do nível h
     * ocupam as posições mais a esquerda da árvore
     * @return
     */
    boolean estahCompleta() ;
    No getSubArvoreEsquerda(Object valor) throws NoNaoEncontradoException;
    No getSubArvoreDireita(Object valor)throws NoNaoEncontradoException;
    TipoNo getTipoNo(Object valor)throws NoNaoEncontradoException;
    int getAltura(Object valor);
    int getNivel(Object valor);
    Object[] getElementos(TipoPercurso tipoPercurso);
    void inserir(Object informacao);
    boolean remover(Object informacao) throws NoNaoEncontradoException;
    No Localizar(Object chave) throws NoNaoEncontradoException;
}
