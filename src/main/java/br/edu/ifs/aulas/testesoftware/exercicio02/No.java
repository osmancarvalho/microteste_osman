package br.edu.ifs.aulas.testesoftware.exercicio02;
/**
 * Created by Francisco on 28/11/2016.
 */
public interface No extends Comparable<No> {
    Object getInfo();
    void setInfo(Object info);
    No getDireita();
    No getEsquerda();
    Void setDireita(No no);
    Void setEsquerda(No no);
}
