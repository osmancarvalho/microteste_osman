package br.edu.ifs.aulas.testesoftware.exercicio02;

/**
 * Created by Francisco on 28/11/2016.
 */
public enum TipoPercurso {
    InOrdem,
    PosOrdem,
    PreOrdem
}
